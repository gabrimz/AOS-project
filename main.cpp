#include <cstdio>
#include <cmath>
#include "miosix.h"
#include "lis3dsh.h"

#define DISTANCE(axes) (sqrt(axes.x*axes.x + axes.y*axes.y + axes.z*axes.z))

using namespace std;
using namespace miosix;

typedef Gpio<GPIOD_BASE, 12> greenLed;
typedef Gpio<GPIOD_BASE, 13> orangeLed;
typedef Gpio<GPIOD_BASE, 14> redLed;
typedef Gpio<GPIOD_BASE, 15> blueLed;

typedef Gpio<GPIOA_BASE, 0>  userButton;

static bool isButtonPressed = false;
static unsigned int pressCount = 0;

static Lis3dsh lis;

static void poll(bool enabled)
{
    if (enabled) {
        orangeLed::high();
        lis.startPolling();
        printf("## Start: interval=%uus avg_samples=%u ##\n",
               lis.samplingInterval, lis.avgSamplesNum);
    } else {
        orangeLed::low();
        lis.stopPolling();
        printf("## Stop ##\n");

        Position finalPosition = lis.getPosition();
        printf("Final position: x=%f, y=%f, z=%f\n"
               "Relative distance covered: %f cm\n",
               finalPosition.x, finalPosition.y, finalPosition.z,
               DISTANCE(finalPosition) * 100);
    }
}

static void buttonPressedAction()
{
    blueLed::high();
    if (pressCount % 2) {
        poll(false);
    }
}

static void buttonReleasedAction()
{
    blueLed::low();
    if (++pressCount % 2) {
        /*
         * Add a delay to ensure that the accelerometer is not
         * moving because of the button press
         */
        usleep(100000);
        poll(true);
    }
}

static void handleDoubletap(void *args)
{
    Thread *checkCalibrationStatusThread = static_cast<Thread *>(args);
    for(;;) {
        lis.waitForDoubletap();
        pressCount++;
        poll(!lis.isPolling());
        checkCalibrationStatusThread->wakeup();
    }
}

static void checkCalibration(void *)
{
    for(;;) {
        if (!lis.isPolling()) {
            greenLed::low();
            Thread::wait();
            continue;
        }
        if (lis.isCalibrating())
            greenLed::high();
        else
            greenLed::low();
        usleep(lis.samplingInterval);
    }
}

int main()
{
    lis.setAccelOutputEnabled(true);
    lis.setVelOutputEnabled(true);
    lis.setPosOutputEnabled(true);

    greenLed::mode(Mode::OUTPUT);
    orangeLed::mode(Mode::OUTPUT);
    redLed::mode(Mode::OUTPUT);
    blueLed::mode(Mode::OUTPUT);

    userButton::mode(Mode::INPUT);

    Thread *checkCalibrationStatusThread = Thread::create(checkCalibration, STACK_MIN);
    Thread::create(handleDoubletap, STACK_MIN * 3, 0, checkCalibrationStatusThread, Thread::DEFAULT);

    for (;;) {
        bool currentStatus = userButton::value();
        if (currentStatus != isButtonPressed) {
            isButtonPressed = currentStatus;
            if (isButtonPressed)
                buttonPressedAction();
            else
                buttonReleasedAction();
            checkCalibrationStatusThread->wakeup();
        }
        usleep(10000);
    }

    return 0;
}
