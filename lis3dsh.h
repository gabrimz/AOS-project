#ifndef LIS3DSH
#define LIS3DSH

#include "accelbase.h"

#include <miosix.h>

#define ARRAY_SIZE(arr) (sizeof(arr)/sizeof(0[arr]))

#define GRAVITY_EARTH  (9.80665f)
#define FSR(fs)        (2 * fs * GRAVITY_EARTH)
#define LSB(fs)        (FSR(fs) / 32768) /* 15 bits */

static const int defaultSamplingInterval = 10000;
static const int defaultAvgSamplesNum = 10;
static const int defaultcalibrationsNum = 5;

/*
 * This class allows to configure the LIS3DSH accelerometer to measure its
 * current acceleration, detect double taps and measure its velocity and position.
 */
class Lis3dsh : public AccelBase {
public:
    Lis3dsh(int interval = defaultSamplingInterval,
            int avgSamplesNum = defaultAvgSamplesNum,
            int calibrationsNum = defaultcalibrationsNum,
            bool roundInterval = true);
    void startPolling();
    void stopPolling();
    bool isPolling();

    int getRoundedInterval(int interval_us);

    void waitForDoubletap();

private:
    void configureSensor();
    miosix::Thread *pollingThread;
    bool isEnabled;
    uint8_t curOdr;
    uint8_t getClosestOdr(int interval_us);
    uint16_t millisecondsToLsb(int ms);

    uint8_t getAccelStatus();
    float getAccelX();
    float getAccelY();
    float getAccelZ();
    Acceleration getAccel();

    miosix::FastMutex mtx;

    static void pollSensorData(void *context);

    class Spi {
    public:
        void config();
        uint8_t read(uint8_t address);
        void write(uint8_t address, uint8_t out);

    private:
        uint8_t transfer(uint8_t address, uint8_t out);
        miosix::FastMutex mtx;
    } spi;

    class IRQhandler {
    public:
        void configureAccInterrupt();
        void waitForInt1();
    } irqHandler;
};

#endif
