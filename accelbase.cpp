#include <cstdio>
#include "accelbase.h"

#define NS (1/1000000.f)

using namespace miosix;

#define MIN(x, y) ((x < y) ? y : x)

/**
 * \param samplingInterval sampling interval expressed in microseconds
 * \param avgSamplesNum number of samples to use to calculate average values
 * of the acceleration. Negative values will be discarded.
 * \param calibrationSamplesNum number of samples used for the calibration.
 * Each calibration sample is obtained averaging AccelBase::avgSamplesNum samples.
 */
AccelBase::AccelBase(int samplingInterval, int avgSamplesNum, int calibrationSamplesNum):
        samplingInterval(MIN(samplingInterval, 1)),
        avgSamplesNum(MIN(avgSamplesNum, 1)),
        calibrationSamplesNum(MIN(calibrationSamplesNum, 1)),
        calibrationSamplesCount(0),
        accelOutputEnabled(false),
        velOutputEnabled(false),
        posOutputEnabled(false)
{
    reset();
    processDataThread = Thread::create(checkSamples, STACK_MIN * 10, 0, this, Thread::DEFAULT);
}

/**
 * \brief Clear data
 *
 * Delete all the queued samples and calculated values.
 */
void AccelBase::reset()
{
    mtxQueue.lock();
    data.clear();
    mtxQueue.unlock();

    mtxData.lock();
    resetVectorAxes(curAcceleration);
    resetVectorAxes(prevAcceleration);
    resetVectorAxes(curVelocity);
    resetVectorAxes(prevVelocity);
    resetVectorAxes(curPosition);
    resetVectorAxes(prevPosition);
    resetVectorAxes(gravity);
    calibrationSamplesCount = 0;
    mtxData.unlock();
}

/**
 * \brief Add new acceleration sample
 *
 * The new sample is processed asynchronously.
 */
void AccelBase::addData(Acceleration data)
{
    mtxQueue.lock();
    this->data.push_back(data);
    mtxQueue.unlock();
    processDataThread->wakeup();
}

void AccelBase::checkSamples(void *context)
{
    AccelBase *accelData = static_cast<AccelBase *>(context);
    for(;;) {
        accelData->processData();
        Thread::wait();
    }
}

/**
 * \brief Process acceleration samples
 *
 * This method will process the currently queued acceleration samples. If not
 * enough samples are available, it returns immediately.
 *
 * \returns true if enough samples were available to be processed
 */
bool AccelBase::processData()
{
    mtxQueue.lock();
    if (data.size() < avgSamplesNum) {
        mtxQueue.unlock();
        return false;
    }
    Acceleration avgAcceleration = { 0, 0, 0 };
    unsigned int i = 0;
    for (std::list<Acceleration>::iterator it = data.begin();
            i < avgSamplesNum && it != data.end(); i++) {
        avgAcceleration.x += it->x;
        avgAcceleration.y += it->y;
        avgAcceleration.z += it->z;
        it = data.erase(it);
    }
    mtxQueue.unlock();

    avgAcceleration.x /= avgSamplesNum;
    avgAcceleration.y /= avgSamplesNum;
    avgAcceleration.z /= avgSamplesNum;

    mtxData.lock();

    if (calibrationSamplesCount < calibrationSamplesNum) {
        calibrationSamplesCount++;
        gravity.x += avgAcceleration.x;
        gravity.y += avgAcceleration.y;
        gravity.z += avgAcceleration.z;
        if (calibrationSamplesCount == calibrationSamplesNum) {
            gravity.x /= calibrationSamplesNum;
            gravity.y /= calibrationSamplesNum;
            gravity.z /= calibrationSamplesNum;
        }
        mtxData.unlock();
        return true;
    }

    avgAcceleration.x -= gravity.x;
    avgAcceleration.y -= gravity.y;
    avgAcceleration.z -= gravity.z;

    float dt = avgSamplesNum * samplingInterval * NS;

    prevAcceleration = curAcceleration;
    curAcceleration = avgAcceleration;

    prevVelocity = curVelocity;
    curVelocity.x += (prevAcceleration.x + (curAcceleration.x - prevAcceleration.x) / 2) * dt;
    curVelocity.y += (prevAcceleration.y + (curAcceleration.y - prevAcceleration.y) / 2) * dt;
    curVelocity.z += (prevAcceleration.z + (curAcceleration.z - prevAcceleration.z) / 2) * dt;

    prevPosition = curPosition;
    curPosition.x += (prevVelocity.x + (curVelocity.x - prevVelocity.x) / 2) * dt;
    curPosition.y += (prevVelocity.y + (curVelocity.y - prevVelocity.y) / 2) * dt;
    curPosition.z += (prevVelocity.z + (curVelocity.z - prevVelocity.z) / 2) * dt;

    mtxData.unlock();

    if (accelOutputEnabled)
        printf("a=(%+f %+f %+f)\n", avgAcceleration.x, avgAcceleration.y, avgAcceleration.z);
    if (velOutputEnabled)
        printf("v=(%+f %+f %+f)\n", curVelocity.x, curVelocity.y, curVelocity.z);
    if (posOutputEnabled)
        printf("p=(%+f %+f %+f)\n", curPosition.x, curPosition.y, curPosition.z);

    return true;
}

/**
 * \brief Flush current samples queue
 *
 * This method force a flush of all the samples waiting to be processed. It
 * returns only once all the samples have been processed. If the queue does
 * not contain enough samples, these will be place in a secondary queue and
 * can be retrieved with AccelBase::getLastDiscardedSamples().
 */
void AccelBase::flushQueue()
{
    while (processData());
    mtxData.lock();
    /* Move remaining items */
    discardedData.clear();
    swap(discardedData, data);
    mtxData.unlock();
}

/**
 * \brief Get current acceleration
 *
 * This method returns the current acceleration without the Earth's gravity
 * components.
 */
Acceleration AccelBase::getAcceleration()
{
    Lock<FastMutex> l(mtxData);
    return curAcceleration;
}

/**
 * \brief Get current velocity
 *
 * This method returns the current velocity, obtained integrating the calculated
 * acceleration.
 */
Velocity AccelBase::getVelocity()
{
    Lock<FastMutex> l(mtxData);
    return curVelocity;
}

/**
 * \brief Get current positon
 *
 * This method returns the current position, obtained integrating the calculated
 * velocity.
 */
Position AccelBase::getPosition()
{
    Lock<FastMutex> l(mtxData);
    return curPosition;
}

/**
 * \brief Get discarded samples
 *
 * \returns list populated with the acceleration samples discarded by the
 * last AccelBase::flushQueue() call.
 */
std::list<Acceleration> AccelBase::getLastDiscardedSamples()
{
    Lock<FastMutex> l(mtxData);
    return discardedData;
}

/**
 * \brief Check if sensor is calibrating
 *
 * \returns whether the sensor is calibrating.
 */
bool AccelBase::isCalibrating()
{
    Lock<FastMutex> l(mtxData);
    return calibrationSamplesCount < calibrationSamplesNum;
}

/**
 * \brief Enable or disable the verbose acceleration output
 *
 * \param enabled Whether to enable the verbose acceleration output.
 */
void AccelBase::setAccelOutputEnabled(bool enabled)
{
    Lock<FastMutex> l1(mtxQueue);
    Lock<FastMutex> l2(mtxData);
    accelOutputEnabled = enabled;
}

/**
 * \returns whether the verbose acceleration output is enabled.
 */
bool AccelBase::getAccelOutputEnabled()
{
    Lock<FastMutex> l1(mtxQueue);
    Lock<FastMutex> l2(mtxData);
    return accelOutputEnabled;
}

/**
 * \brief Enable or disable the verbose velocity output
 *
 * \param enabled Whether to enable the verbose velocity output.
 */
void AccelBase::setVelOutputEnabled(bool enabled)
{
    Lock<FastMutex> l1(mtxQueue);
    Lock<FastMutex> l2(mtxData);
    velOutputEnabled = enabled;
}

/**
 * \returns whether the verbose velocity output is enabled.
 */
bool AccelBase::getVelOutputEnabled()
{
    Lock<FastMutex> l1(mtxQueue);
    Lock<FastMutex> l2(mtxData);
    return velOutputEnabled;
}

/**
 * \brief Enable or disable the verbose position output
 *
 * \param enabled Whether to enable the verbose position output.
 */
void AccelBase::setPosOutputEnabled(bool enabled)
{
    Lock<FastMutex> l1(mtxQueue);
    Lock<FastMutex> l2(mtxData);
    posOutputEnabled = enabled;
}

/**
 * \returns whether the verbose position output is enabled.
 */
bool AccelBase::getPosOutputEnabled()
{
    Lock<FastMutex> l1(mtxQueue);
    Lock<FastMutex> l2(mtxData);
    return posOutputEnabled;
}
