#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <vector>
#include <QAction>
#include <QByteArray>
#include <QMainWindow>
#include <QGroupBox>
#include <QLabel>
#include <qwt/qwt_legend.h>
#include <qwt/qwt_plot.h>
#include <qwt/qwt_plot_grid.h>
#include <qwt/qwt_plot_curve.h>
#include <qwt/qwt_plot_renderer.h>
#include <qwt/qwt_scale_widget.h>
#include "QAsyncSerial.h"

class MainWindow : public QMainWindow
{
Q_OBJECT
public:
    MainWindow();
    ~MainWindow();

private slots:
    void update(QString data, bool replot = true);
    void openLog();
    void saveLog();
    void exportPlots();

private:
    void resetData();

    QWidget *mainWidget;

    void createMenu();
    QMenu *fileMenu;
    QAction *openAction;
    QAction *saveAction;
    QAction *exportAction;

    QByteArray log;

    QLabel *infoAccel;
    QLabel *infoVel;
    QLabel *infoPos;

    QwtPlot *plotAccel;
    QwtPlot *plotVel;

    QwtPlotCurve *curveAccelX;
    QwtPlotCurve *curveAccelY;
    QwtPlotCurve *curveAccelZ;
    QwtPlotCurve *curveVelX;
    QwtPlotCurve *curveVelY;
    QwtPlotCurve *curveVelZ;

    std::vector<double> dataAccelX;
    std::vector<double> dataAccelY;
    std::vector<double> dataAccelZ;
    std::vector<double> timestampsAccel;
    std::vector<double> dataVelX;
    std::vector<double> dataVelY;
    std::vector<double> dataVelZ;
    std::vector<double> timestampsVel;

    int avgSamplesNum;
    int samplingInterval;

    QAsyncSerial *serial;
};

#endif //MAINWINDOW_H
