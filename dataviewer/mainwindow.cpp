#include "mainwindow.h"
#include <QDateTime>
#include <QFont>
#include <QFileDialog>
#include <QGridLayout>
#include <QMessageBox>
#include <QMenuBar>
#include <QRegExp>
#include <QStandardPaths>

using namespace std;

MainWindow::MainWindow(): samplingInterval(100), avgSamplesNum(10)
{
    this->setWindowTitle("Accelerometer data viewer");

    mainWidget = new QWidget(this);
    setCentralWidget(mainWidget);
    createMenu();

    plotAccel = new QwtPlot(mainWidget);
    plotAccel->setTitle("Acceleration");
    plotAccel->setCanvasBackground(Qt::white);
    plotAccel->setMinimumSize(400, 200);

    QFont axisFont;
    axisFont.setPointSize(8);
    QwtText axisTitle;
    axisTitle.setFont(axisFont);

    QwtLegend *legendAccel = new QwtLegend(mainWidget);
    plotAccel->insertLegend(legendAccel, QwtPlot::LeftLegend, 1.0);
    axisTitle.setText("acceleration (m/s²)");
    plotAccel->axisWidget(QwtPlot::yLeft)->setTitle(axisTitle);
    axisTitle.setText("time (s)");
    plotAccel->axisWidget(QwtPlot::xBottom)->setTitle(axisTitle);

    QwtPlotGrid *gridAccel = new QwtPlotGrid();
    gridAccel->setPen(Qt::gray, 1);
    gridAccel->attach(plotAccel);

    curveAccelX = new QwtPlotCurve();
    curveAccelX->setTitle("x-axis");
    curveAccelX->setPen(Qt::red, 1);
    curveAccelX->attach(plotAccel);

    curveAccelY = new QwtPlotCurve();
    curveAccelY->setTitle("y-axis");
    curveAccelY->setPen(Qt::green, 1);
    curveAccelY->attach(plotAccel);

    curveAccelZ = new QwtPlotCurve();
    curveAccelZ->setTitle("z-axis");
    curveAccelZ->setPen(Qt::blue, 1);
    curveAccelZ->attach(plotAccel);

    plotVel = new QwtPlot(mainWidget);
    plotVel->setTitle("Velocity");
    plotVel->setCanvasBackground(Qt::white);
    plotVel->setMinimumSize(400, 200);
    axisTitle.setText("velocity (m/s)");
    plotVel->axisWidget(QwtPlot::yLeft)->setTitle(axisTitle);
    axisTitle.setText("time (s)");
    plotVel->axisWidget(QwtPlot::xBottom)->setTitle(axisTitle);

    QwtLegend *legendVel = new QwtLegend(mainWidget);
    plotVel->insertLegend(legendVel, QwtPlot::LeftLegend, 1.0);

    QwtPlotGrid *gridVel = new QwtPlotGrid();
    gridVel->setPen(Qt::gray, 1);
    gridVel->attach(plotVel);

    curveVelX = new QwtPlotCurve();
    curveVelX->setTitle("x-axis");
    curveVelX->setPen(Qt::red, 1);
    curveVelX->attach(plotVel);

    curveVelY = new QwtPlotCurve();
    curveVelY->setTitle("y-axis");
    curveVelY->setPen(Qt::green, 1);
    curveVelY->attach(plotVel);

    curveVelZ = new QwtPlotCurve();
    curveVelZ->setTitle("z-axis");
    curveVelZ->setPen(Qt::blue, 1);
    curveVelZ->attach(plotVel);

    infoAccel = new QLabel("Current acceleration:\n0 m/s²");
    infoAccel->setAlignment(Qt::AlignCenter);
    infoAccel->setMinimumWidth(50);
    infoVel = new QLabel("Current speed:\n0 m/s");
    infoVel->setAlignment(Qt::AlignCenter);
    infoVel->setMinimumWidth(50);
    infoPos = new QLabel("Current position: x=0 y=0 z=0\n"
            "Relative distance: 0 cm");
    infoPos->setAlignment(Qt::AlignCenter);

    QGridLayout *layout = new QGridLayout(this);
    layout->addWidget(plotAccel, 1, 0);
    layout->addWidget(infoAccel, 1, 1);
    layout->addWidget(plotVel, 2, 0);
    layout->addWidget(infoVel, 2, 1);
    layout->addWidget(infoPos, 3, 0, 1, 2, Qt::AlignHCenter);
    mainWidget->setLayout(layout);

    serial = new QAsyncSerial("/dev/ttyUSB0", 19200);
    connect(serial, SIGNAL(lineReceived(QString)), this, SLOT(update(QString)));

    this->show();

    if (!serial->isOpen()) {
        QMessageBox msg(this);
        msg.setIcon(QMessageBox::Warning);
        msg.setText(tr("Could not open serial device"));
        msg.exec();
    }
}

MainWindow::~MainWindow()
{
    delete serial;
}

void MainWindow::createMenu()
{
    openAction = new QAction(tr("&Open log..."), this);
    saveAction = new QAction(tr("&Save log as..."), this);
    exportAction = new QAction(tr("&Export plots as PDF..."), this);

    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(openAction);
    fileMenu->addAction(saveAction);
    fileMenu->addAction(exportAction);

    connect(openAction, SIGNAL(triggered()), this, SLOT(openLog()));
    connect(saveAction, SIGNAL(triggered()), this, SLOT(saveLog()));
    connect(exportAction, SIGNAL(triggered()), this, SLOT(exportPlots()));
}

void MainWindow::openLog()
{
    QString path = QFileDialog::getOpenFileName(this);
    QFile inputFile(path);
    resetData();
    if (inputFile.open(QIODevice::ReadOnly)) {
        QTextStream in(&inputFile);
        while (!in.atEnd())
            update(in.readLine(), false);
        inputFile.close();
        plotAccel->replot();
        plotVel->replot();
    }
}

void MainWindow::saveLog()
{
    QString path = QFileDialog::getSaveFileName(this, "Save As", "output.log");
    if (!path.isNull()) {
        QFile logFile(path);
        if (!logFile.open(QIODevice::WriteOnly)) {
            QMessageBox msg(this);
            msg.setIcon(QMessageBox::Warning);
            msg.setText(tr("Could not save log"));
            msg.exec();
            return;
        }
        logFile.write(log);
        logFile.close();
    }
}

void MainWindow::exportPlots()
{
    const QString path(QFileDialog::getExistingDirectory(this, tr("Save plots in..."),
            QStandardPaths::standardLocations(QStandardPaths::HomeLocation).at(0),
            QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks));

    const QString now(QDateTime::currentDateTime().toString("yyyyMMdd-hhmmss"));
    const QString plotAccelName = QString("%1/plot_accel-%2.pdf").arg(path).arg(now);
    const QString plotVelName = QString("%1/plot_vel-%2.pdf").arg(path).arg(now);

    QwtPlotRenderer renderer;
    renderer.renderDocument(plotAccel, plotAccelName, QSizeF(197, 110), 85);
    renderer.renderDocument(plotVel, plotVelName, QSizeF(197, 110), 85);
}

void MainWindow::resetData()
{
    dataAccelX.clear();
    dataAccelY.clear();
    dataAccelZ.clear();
    timestampsAccel.clear();
    dataVelX.clear();
    dataVelY.clear();
    dataVelZ.clear();
    timestampsVel.clear();
    log.clear();
}

void MainWindow::update(QString data, bool replot)
{
    QRegExp rx1("([avp])=\\(([-+]?\\d+\\.\\d+) ([-+]?\\d+\\.\\d+) ([-+]?\\d+\\.\\d+)\\)");
    QRegExp rx2("## Start: interval=(\\d+)us avg_samples=(\\d+) ##");

    log.append(data + "\n");

    if (rx1.indexIn(data, 0) != -1) {
        QString type = rx1.cap(1);
        double x = rx1.cap(2).toDouble();
        double y = rx1.cap(3).toDouble();
        double z = rx1.cap(4).toDouble();

        double interval_delta = samplingInterval * avgSamplesNum / 1000000.f;
        if (type == "a") {
            double module = sqrt(x*x + y*y + z*z);
            dataAccelX.push_back(x);
            dataAccelY.push_back(y);
            dataAccelZ.push_back(z);
            timestampsAccel.push_back(timestampsAccel.empty() ? 0 : timestampsAccel.back() + interval_delta);
            curveAccelX->setRawSamples(timestampsAccel.data(), dataAccelX.data(), timestampsAccel.size());
            curveAccelY->setRawSamples(timestampsAccel.data(), dataAccelY.data(), timestampsAccel.size());
            curveAccelZ->setRawSamples(timestampsAccel.data(), dataAccelZ.data(), timestampsAccel.size());
            infoAccel->setText(tr("Current acceleration:\n%1 m/s²").arg(module));
            if (replot)
                plotAccel->replot();
        } else if (type == "v") {
            double module = sqrt(x*x + y*y + z*z);
            dataVelX.push_back(x);
            dataVelY.push_back(y);
            dataVelZ.push_back(z);
            timestampsVel.push_back(timestampsVel.empty() ? 0 : timestampsVel.back() + interval_delta);
            curveVelX->setRawSamples(timestampsVel.data(), dataVelX.data(), timestampsVel.size());
            curveVelY->setRawSamples(timestampsVel.data(), dataVelY.data(), timestampsVel.size());
            curveVelZ->setRawSamples(timestampsVel.data(), dataVelZ.data(), timestampsVel.size());
            infoVel->setText(tr("Current speed:\n%1 m/s").arg(module));
            plotVel->replot();
            if (replot)
                plotVel->replot();
        } else if (type == "p") {
            double distance = sqrt(x*x + y*y + z*z) * 100; // cm
            infoPos->setText(tr("Current position: x=%1 y=%2 z=%3\n"
                    "Relative distance: %4 cm")
                    .arg(x).arg(y).arg(z).arg(distance));
        }
    } else if (rx2.indexIn(data, 0) != -1) {
        resetData();
        log.append(data + "\n");
        samplingInterval = rx2.cap(1).toInt();
        avgSamplesNum = rx2.cap(2).toInt();
    }
}
