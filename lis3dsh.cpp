#include <cstdio>

#include "lis3dsh.h"
#include "lis3dsh_reg.h"

using namespace std;
using namespace miosix;

static const struct {
   uint8_t registerMask;
   int mHz;
   int us;
} odrTable[] = {
    /* Order is important */
    { ODR1600, 1600000, 625 },
    { ODR800,  800000,  1250 },
    { ODR400,  400000,  2500 },
    { ODR100,  100000,  10000 },
    { ODR50,   50000,   20000 },
    { ODR25,   25000,   40000 },
    { ODR12,   12500,   80000 },
    { ODR6,    6250,    160000 },
    { ODR3,    3125,    320000 },
};

/**
 * @copydoc AccelBase::AccelBase()
 *
 * \param roundInterval whether or not to round the given sampling interval as
 * with \ref Lis3dsh::getRoundedInterval()
 */
Lis3dsh::Lis3dsh(int samplingInterval, int avgSamplesNum,
                 int calibrationSamplesNum, bool roundInterval):
        AccelBase(roundInterval ? getRoundedInterval(samplingInterval) : samplingInterval,
                  avgSamplesNum, calibrationSamplesNum),
        isEnabled(false)
{
    spi.config();
    configureSensor();

    pollingThread = Thread::create(pollSensorData, STACK_MIN, PRIORITY_MAX - 1, this, Thread::DEFAULT);
}

void Lis3dsh::pollSensorData(void *context)
{
    Lis3dsh *lis = static_cast<Lis3dsh *>(context);
    for (;;) {
        if (!lis->isPolling()) {
            Thread::wait();
            continue;
        }
        if (lis->getAccelStatus() & 0x4) {
            lis->addData(lis->getAccel());
        }
        usleep(lis->samplingInterval);
    }
}

static size_t getOdrTableIndex(int interval_us)
{
    if (interval_us < odrTable[0].us)
        printf("\nWarning: the smallest sampling interval supported is %dus.\n",
               odrTable[0].us);

    size_t i;
    for (i = 0; i < ARRAY_SIZE(odrTable) - 1; i++) {
        if (interval_us < odrTable[i + 1].us)
            break;
    }
    return i;
}

uint8_t Lis3dsh::getClosestOdr(int interval_us)
{
    return odrTable[getOdrTableIndex(interval_us)].registerMask;
}

/**
 * \brief Round the given sampling interval
 *
 * \param interval_us sampling interval to round expressed in microseconds.
 *
 * \returns Rounded sampling interval in microseconds. The returned sampling
 * interval is always smaller or equal to the given interval.
 */
int Lis3dsh::getRoundedInterval(int interval_us)
{
    return odrTable[getOdrTableIndex(interval_us)].us;
}

void Lis3dsh::configureSensor()
{
    uint8_t data;

    data = ENABLE_XYZ;
    curOdr = getClosestOdr(samplingInterval);
    data |= curOdr;
    spi.write(CTRL_REG4, data);

    if (curOdr != ODR100)
        printf("\nDouble tap detection works correctly when ODR is 100Hz.\n"
               "Select polling interval value between t: 10000 <= t < 20000\n\n");

    /* Configure accelerometer for double tap detection */
    spi.write(CTRL_REG1, 0x1);
    spi.write(CTRL_REG3, 0x48);
    spi.write(CTRL_REG5, 0);
    spi.write(TIM4_1, millisecondsToLsb(20));
    spi.write(TIM3_1, millisecondsToLsb(10));
    spi.write(TIM2_1L, millisecondsToLsb(500));
    spi.write(TIM1_1L, millisecondsToLsb(70));
    spi.write(THRS2_1, 0x55);
    spi.write(THRS1_1, 0x55);
    spi.write(ST1_1, 0x51);
    spi.write(ST1_2, 0x51);
    spi.write(ST1_3, 0x6);
    spi.write(ST1_4, 0x38);
    spi.write(ST1_5, 0x4);
    spi.write(ST1_6, 0x91);
    spi.write(ST1_7, 0x26);
    spi.write(ST1_8, 0x38);
    spi.write(ST1_9, 0x4);
    spi.write(ST1_10, 0x91);
    spi.write(ST1_11, 0x11);
    spi.write(MASK1_A, 0xfc);
    spi.write(SETT1, 0xa1);
    irqHandler.configureAccInterrupt();
}

/**
 * \brief Start sampling
 *
 * This method returns immediately if the accelerometer is active.
 */
void Lis3dsh::startPolling()
{
    mtx.lock();
    if (isEnabled) {
        mtx.unlock();
        return;
    }
    isEnabled = true;
    mtx.unlock();

    reset();

    /* Discard first sample since it's usually incorrect */
    getAccel();

    pollingThread->wakeup();
}

/**
 * \brief Stop sampling
 *
 * This method returns immediately if the accelerometer is not active or
 * waits for all the queued acceleration samples to be processed.
 */
void Lis3dsh::stopPolling()
{
    mtx.lock();
    if (!isEnabled) {
        mtx.unlock();
        return;
    }
    isEnabled = false;
    mtx.unlock();
    flushQueue();
}

/**
 * \returns True if the accelerometer is currently sampling
 */
bool Lis3dsh::isPolling()
{
    Lock<FastMutex> l(mtx);
    return isEnabled;
}

uint8_t Lis3dsh::getAccelStatus()
{
    return spi.read(STATUS);
}

float Lis3dsh::getAccelX()
{
    return (int16_t)((spi.read(OUT_X_H) << 8) | spi.read(OUT_X_L)) * LSB(2);
}

float Lis3dsh::getAccelY()
{
    return (int16_t)((spi.read(OUT_Y_H) << 8) | spi.read(OUT_Y_L)) * LSB(2);
}

float Lis3dsh::getAccelZ()
{
    return (int16_t)((spi.read(OUT_Z_H) << 8) | spi.read(OUT_Z_L)) * LSB(2);
}

Acceleration Lis3dsh::getAccel()
{
    Acceleration data;
    data.x = getAccelX();
    data.y = getAccelY();
    data.z = getAccelZ();
    return data;
}

/**
 * \brief Wait for a double tap
 *
 * Block the current thread and wait until a double tap is detected.
 */
void Lis3dsh::waitForDoubletap()
{
    irqHandler.waitForInt1();
    spi.read(OUTS1); /* interrupt is latched, must read OUTS1 */
}

uint16_t Lis3dsh::millisecondsToLsb(int ms)
{
    size_t i;
    for (i = 0; i < ARRAY_SIZE(odrTable); i++)
        if (odrTable[i].registerMask == curOdr)
            break;
    return ms * odrTable[i].mHz / 1000 / 1000;
}
