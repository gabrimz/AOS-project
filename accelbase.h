#ifndef ACCEL_DATA
#define ACCEL_DATA

#include <list>
#include <miosix.h>

/**
 * Structure that groups the components of a vector along the three axes x, y, z.
 */
template <typename T>
struct Axes {
    T x;
    T y;
    T z;
};
typedef struct Axes<float> Velocity;
typedef struct Axes<float> Acceleration;
typedef struct Axes<float> Position;

template<typename T>
void resetVectorAxes(T &v)
{
	v.x = 0;
	v.y = 0;
	v.z = 0;
}

/**
 * This class provides methods to process the acceleration samples of an
 * accelerometer to measure its movements.
 */
class AccelBase {
public:
    AccelBase(int samplingInterval, int avgSamplesNum, int calibrationSamplesNum);
    void reset();

    /**
     * Current sampling interval
     */
    const unsigned int samplingInterval;

    /**
     * Number of samples used for averaging the acceleration
     */
    const unsigned int avgSamplesNum;

    /**
     * Number of samples used for the calibration. Each calibration sample is
     * obtained averaging AccelBase::avgSamplesNum samples.
     */
    const unsigned int calibrationSamplesNum;

    Acceleration getAcceleration();
    Velocity getVelocity();
    Position getPosition();

    std::list<Acceleration> getLastDiscardedSamples();

    void addData(Acceleration data);
    void flushQueue();

    bool isCalibrating();

    void setAccelOutputEnabled(bool enabled);
    bool getAccelOutputEnabled();
    void setVelOutputEnabled(bool enabled);
    bool getVelOutputEnabled();
    void setPosOutputEnabled(bool enabled);
    bool getPosOutputEnabled();

private:
    Acceleration curAcceleration;
    Acceleration prevAcceleration;
    Velocity curVelocity;
    Velocity prevVelocity;
    Position curPosition;
    Position prevPosition;
    Acceleration gravity;
    std::list<Acceleration> data;
    std::list<Acceleration> discardedData;

    bool processData();

    unsigned int calibrationSamplesCount;

    bool accelOutputEnabled;
    bool velOutputEnabled;
    bool posOutputEnabled;

    miosix::Thread *processDataThread;
    miosix::FastMutex mtxQueue;
    miosix::FastMutex mtxData;

    static void checkSamples(void *context);
};

#endif
