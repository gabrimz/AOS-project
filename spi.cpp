#include <miosix.h>
#include "lis3dsh.h"

#define SPI_READ_MASK 0x80

using namespace miosix;

typedef Gpio<GPIOA_BASE, 5> SPI1_SCK;
typedef Gpio<GPIOA_BASE, 6> SPI1_MISO;
typedef Gpio<GPIOA_BASE, 7> SPI1_MOSI;
typedef Gpio<GPIOE_BASE, 3> SPI1_CS;

/**
 * \brief Configure SPI1
 *
 * Configure SPI1 to allow communication with the accelerometer.
 */
void Lis3dsh::Spi::config()
{
    RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
    RCC->AHB1ENR |= RCC_AHB1ENR_GPIOAEN | RCC_AHB1ENR_GPIOEEN;

    SPI1_SCK::mode(Mode::ALTERNATE);
    SPI1_SCK::alternateFunction(5);
    SPI1_SCK::speed(Speed::_50MHz);

    SPI1_MISO::mode(Mode::ALTERNATE);
    SPI1_MISO::alternateFunction(5);
    SPI1_SCK::speed(Speed::_50MHz);

    SPI1_MOSI::mode(Mode::ALTERNATE);
    SPI1_MOSI::alternateFunction(5);
    SPI1_SCK::speed(Speed::_50MHz);

    SPI1_CS::mode(Mode::OUTPUT);
    SPI1_CS::high();
    SPI1_SCK::speed(Speed::_50MHz);

    SPI1->CR1 = SPI_CR1_SSM | SPI_CR1_SSI | SPI_CR1_MSTR | SPI_CR1_SPE;
}

uint8_t Lis3dsh::Spi::transfer(uint8_t address, uint8_t out)
{
    SPI1_CS::low();

    SPI1->DR = address;
    while ((SPI1->SR & SPI_SR_TXE) == 0);
    while ((SPI1->SR & SPI_SR_RXNE) == 0);
    (void)SPI1->DR;

    SPI1->DR = out;
    while ((SPI1->SR & SPI_SR_RXNE) == 0);
    while ((SPI1->SR & SPI_SR_TXE) == 0);
    while ((SPI1->SR & SPI_SR_BSY) != 0);

    SPI1_CS::high();

    return SPI1->DR;
}

/**
 * Read device register
 *
 * \param address address of the register to read
 */
uint8_t Lis3dsh::Spi::read(uint8_t address)
{
    Lock<FastMutex> l(mtx);
    return transfer(SPI_READ_MASK | address, 0);
}

/**
 * Write device register
 *
 * \param address address of the register to write
 * \param out value to write
 */
void Lis3dsh::Spi::write(uint8_t address, uint8_t out)
{
    Lock<FastMutex> l(mtx);
    transfer(address, out);
}
