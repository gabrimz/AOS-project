#ifndef LIS3DSH_REG_H
#define LIS3DSH_REG_H

#define CTRL_REG4 0x20
#define CTRL_REG1 0x21
#define CTRL_REG2 0x22
#define CTRL_REG3 0x23
#define CTRL_REG5 0x24
#define CTRL_REG6 0x25

#define STATUS  0x27
#define OUT_X_L 0x28
#define OUT_X_H 0x29
#define OUT_Y_L 0x2a
#define OUT_Y_H 0x2b
#define OUT_Z_L 0x2c
#define OUT_Z_H 0x2d

#define ST1_1   0x40
#define ST1_2   0x41
#define ST1_3   0x42
#define ST1_4   0x43
#define ST1_5   0x44
#define ST1_6   0x45
#define ST1_7   0x46
#define ST1_8   0x47
#define ST1_9   0x48
#define ST1_10  0x48
#define ST1_11  0x49

#define TIM4_1  0x50
#define TIM3_1  0x51
#define TIM2_1L 0x52
#define TIM1_1L 0x54

#define THRS2_1 0x56
#define THRS1_1 0x57

#define MASK1_A 0x5a
#define SETT1   0x5b

#define OUTS1   0x5f

#define ODR_OFF 0x00  /* Off */
#define ODR3    0x10  /* 3.125 Hz */
#define ODR6    0x20  /* 6.25 Hz */
#define ODR12   0x30  /* 12.5 Hz */
#define ODR25   0x40  /* 25 Hz */
#define ODR50   0x50  /* 50 Hz */
#define ODR100  0x60  /* 100 Hz */
#define ODR400  0x70  /* 400 Hz */
#define ODR800  0x80  /* 800 Hz */
#define ODR1600 0x90  /* 1600 Hz */

#define ENABLE_XYZ 0x07

#define G_2G  0x00
#define G_4G  0x10
#define G_8G  0x20
#define G_16G 0x30

#endif /* LIS3DSH_REG_H */
